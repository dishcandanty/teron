# Teron

I wants a stupid simple class initializer. Here it is :)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'teron'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install teron

## Usage

Example class

```ruby
class Book < Teron
  field :name, default: 'Way of Kings'
  field :number, default: -> { rand(1..3)}
end

b = Book.new

class Page < Teron
  field :diff, default: 'Archive', dump: false
  field :thing, default: 3
  field :other
end

page = Page.new
```

**Dump** Use marshal/dump/load when saving/unloading

# Record Keeping

**Highly experimental**

```ruby
Book.where(number: 1, name: 'Way of Kings') # Match All
Book.find_by(number: 1) # First
Book.first
Book.last
Book.all

book = Book.new
book.save!
book.destroy!
book.reload
```

# Association

## Methods

| Name       | Params | Description                                        |
| ---------- | ------ | -------------------------------------------------- |
| has_many   | Symbol | List of Owners - Capitalized Singularized Constant |
| has_one    | Symbol | Inverse                                            |
| belongs_to | Symbol | Inverse of `has_many`                              |

### Has Many

## Methods

Using `:pages` as an example

| Name                | Description                     |
| ------------------- | ------------------------------- |
| `pages`             | List of Page.find_by            |
| `pages_add`         | :pages.push Page                |
| `pages_create`      | Page.new, pages_add, Page.save! |
| `pages_add`         | Page.new.save!                  |
| `pages_destroy_all` | Page.each(&:destroy!)           |
|                     |

- `pages :pages =>
- Base Method for whatever you defined. :pages => List of Page.find_by
- Base + \_create. :pages => :pages_create => Page.new

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/davinwalker/teron. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/davinwalker/teron/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Teron project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/davinwalker/teron/blob/main/CODE_OF_CONDUCT.md).

## Others (better libraries)

https://github.com/applift/fast_attributes

dry-rb
active_type

https://github.com/pawurb/smart_init

# TODO

- Add freaking real testing / test.rb sucks
- `default: false` fails.
