# Associations for Has Many
module TeronAssociation
  # The Object to be included
  def belongs_to(class_name, param_options = {})
    # Should Inverse Object Be Updated
    inverse = param_options[:inverse] || true

    field "#{class_name}_id".to_sym

    # ==================================
    # Accessor
    # ==================================
    define_method(class_name) do
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      # Get Entry Id
      klass_obj_id = send("#{class_name}_id".to_sym)
      klass.find klass_obj_id
    end

    # ==================================
    # Add
    # ==================================
    define_method("#{class_name}=") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      send("#{class_name}_id=", klass_obj.id)
      save!
    end

    # ==================================
    # Destroy
    # ==================================
    define_method('destroy!') do
      return unless inverse

      # Get Association Name
      klass_single_name = self.class.name.downcase.singularize
      klass_plural_name = self.class.name.downcase.pluralize

      parent = send(class_name)
      return unless parent

      # ==================================
      # Has One vs Has Many
      # ==================================
      # Has one
      if parent.respond_to?("#{klass_single_name}_remove".to_sym)
        parent.send("#{klass_single_name}_remove", self)
        # Has Many
      elsif parent.respond_to?("#{klass_plural_name}_remove".to_sym)
        parent.send("#{klass_plural_name}_remove", self)
      end

      super()
    end
  end
end
