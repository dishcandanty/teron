# Associations for Has Many
module TeronAssociation
  # The Object to do the including
  def has_one(class_name, param_options = {})
    # @relationships ||= []

    # Should Inverse Object Be Updated
    inverse = if param_options.key?(:inverse)
                param_options[:inverse]
              else
                true
              end

    field "#{class_name}_id".to_sym

    # ==================================
    # Accessor
    # ==================================
    define_method(class_name) do
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      # Get Entry Id
      klass_one = send("#{class_name}_id".to_sym)
      return klass_one if klass_one.nil?

      klass.find klass_one
    end

    # ==================================
    # Add
    # ==================================
    define_method("#{class_name}=") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      send("#{class_name}_id=", klass_obj.id)
      save!
    end

    # ==================================
    # Remove (Meta)
    # Don't think this whould be called directly
    # ==================================
    define_method("#{class_name}_remove") do |klass_obj|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize

      # Validate Correct Klass Object
      raise "Invalid #{klass}; Received #{klass_obj.class})" unless klass_obj.instance_of?(klass)

      klass_name = self.class.name.downcase.singularize

      # Get Entry Id
      # klass_one = send("#{class_name}_id".to_sym)
      # return klass_one if klass_one.nil?

      klass_obj.send("#{klass_name}_id=", nil) if inverse
      send("#{class_name}_id=", nil)

      save!
    end

    # ==================================
    # Create
    # ==================================
    define_method("#{class_name}_create") do |opts = {}|
      # Get Constant
      klass = class_name.to_s.capitalize.singularize.classify.constantize
      klass_obj = klass.new(opts)

      send("#{class_name}=", klass_obj)

      # Klass Has One Name
      klass_one_name = self.class.to_s.underscore

      # Update Obj Association
      if inverse
        klass_obj.send("#{klass_one_name}=", self)
        klass_obj.save!
      end

      save!

      klass_obj
    end

    # # ==================================
    # # Destroy All
    # # ==================================
    # define_method("#{class_name}_destroy_all") do
    #   send(class_name).each(&:destroy!) if inverse
    # end

    # class_variable_set(:@@relationships, @relationships)

    # true
  end
end
