# Field Actions
module TeronField
  def field(param_name, param_options = {})
    unless class_variables.include? :@@data
      class_variable_set(:@@data, {})
      class_variable_set(:@@mutex, Mutex.new)
      class_variable_set(:@@object_list, {})
    end

    @params ||= [{
      name: :id,
      options: { default: -> { SecureRandom.uuid }, dump: true }
    }]

    param_options[:dump] = true unless param_options.key? :dump

    @params.push(
      name: param_name,
      options: param_options
    )
    attr_accessor param_name.to_sym

    class_variable_set(:@@params, @params)

    nil
  end
end
