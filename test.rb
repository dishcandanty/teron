require 'teron'
require 'pry'

# ======================================================
# Has Many Testing
# ======================================================
# Demo Book
class Book < Teron
  field :name, default: 'Way of Kings'
  field :number, default: -> { rand(1..3) }

  has_many :pages
  has_one :author

  def rawr
    @rawr ||= 0
    @rawr += 1

    @rawr
  end
end

# Demo Page
class Page < Teron
  field :diff, default: 'Archive'
  field :thing, default: 3
  field :other

  belongs_to :book, class: Book
end

book = Book.new
book.save!
page = book.pages_create
page.save!

puts "True: #{Book.count == 1}, #{Page.count == 1}"

page = Page.new
Book.new.save!
page.save!

puts "True: #{Book.count == 2}, #{Page.count == 2}"

# Remove Page
remove_page = book.pages.first
book.pages_remove remove_page

# Remove Page from Page
page_destroy = book.pages_create
page_destroy.save!
page_destroy.destroy!

puts "Has Many Nil, True: #{remove_page.book.nil?}"
puts "Has Many Empty, True: #{book.pages.empty?}"

# ======================================================
# Instance Variables
# ======================================================

book = Book.new(name: 'Many Pages Test')
book.save!
book.rawr
book.rawr
puts "True: #{book.rawr == 3}"

new_book = Book.find_by(name: 'Many Pages Test')
puts "Instance Variables  True: #{new_book.object_id != book.object_id}"
puts "Instance Variables  True: #{new_book.rawr == 1}"

# ------------------------------------------------------------------------------

# Shared Namespace problems
class Builder < Teron
  field :cards, default: []
end

first_build = Builder.new
first_build.cards.push 1
first_build.cards.count

first_build.cards.object_id

second_build = Builder.new

# Should be FALSE
puts "Instance Variables False: #{first_build.cards.equal?(second_build.cards)}"

# ======================================================
# Has_One Inverse: false
# ======================================================
class FanArt < Teron
  field :name, default: -> { SecureRandom.alphanumeric }
  has_one :book, inverse: false
end

f = FanArt.new
f.save!

# Dump Testing
class User < Teron
  field :ws, dump: false
end

repeat = { name: { value: true } }
user = User.new(ws: repeat)
user.save!
user.reload

# Should be True
puts "True:  #{repeat.equal?(user.ws)}"

# ======================================================
# Has_One Inverse: True (Ensure Sucessful deletion)
# ======================================================
class Movie < Teron
  field :name, default: -> { SecureRandom.alphanumeric }
  has_one :character
end

class Character < Teron
  belongs_to :movie
end

movie = Movie.new
movie.save!

c = movie.character_create
c.save!

puts "Has One, True: #{movie.character.id == c.id}"
puts "Has One Destroy: True: #{movie.character.destroy!}"
puts "Has One Destroyed: Movie True: #{Movie.first.character.nil?}"
puts "Has One Destroyed: Character True: #{Character.count.zero?}"
